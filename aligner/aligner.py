import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

from aligner.helper import *


class Align:

    def __init__(self, query, reference, match_cost=1, miss_cost=0, indel_cost=-1, heavy_consec_indels=False,
                 consec_indel_mult=0.1, backtrack_mode="NeedlemanWunsch"):
        """

        :param string query:
        :param string reference:
        :param int match_cost: score of a match in the alignment
        :param int miss_cost: score of a mismatch in the alignment
        :param int indel_cost: score of a insertion/deletion in the alignment
        :param bool heavy_consec_indels: whether to weight more consecutive indel paths
        :param float consec_indel_mult: float in (0, 1) that multiplies the number of consecutive indel
        """

        self.__backtrack_mode = backtrack_mode

        # Cost values for different aligning actions
        self.__match_cost = match_cost
        self.__miss_cost = miss_cost
        self.__indel_cost = indel_cost
        self.__heavy_consec_indels = heavy_consec_indels
        self.__consec_indel_mult = consec_indel_mult

        # Query and reference strings to be alinged
        self.__query = query
        self.__reference = reference

        # #SCORE STRUCTURES
        # Matrix of scores
        self.__table = None
        # Matrix of scores with header row and col (graph purposes only)
        self.__tot_height = len(self.__query) + 2
        self.__tot_length = len(self.__reference) + 2
        # Matrix that stores the number of indel operations that lead to the score of the node
        self.__indels = None

        # #NETWORKX HELPER VARIABLES
        self.__graph = None
        self.__layout = None

        # List of nodes
        self.__nodes = None
        # List of tuples of Nodes
        self.__edges = []
        # Dict of {Node: Position} in the space (graph purposes only)
        self.__positions = {}
        # Dict of Node: Label (graph purposes only)
        self.__labels = {}
        # List of colors  (graph purposes only)
        self.__colors = None
        # list of list of nodes visited during each traceback
        self.__tracebacks = {
            "paths": [],
            "paths_edges": []
        }

        # Initialize the alignment operations
        self.__init_align()

    def __init_align(self):
        """
        Generates the table from the query and reference sizes, and creates the first row and col associated costs
        """

        # Initialize to 0
        self.__table = np.zeros((len(self.__query) + 1, len(self.__reference) + 1), float)
        self.__indels = np.zeros((len(self.__query) + 1, len(self.__reference) + 1), int)


        # Write also the asociated edges
        for i in range(0, self.__table.shape[1] - 1):
            self.__edges.append((pos_to_index((0, i), self.__tot_length),
                                 pos_to_index((0, i + 1), self.__tot_length)))

        for i in range(0, self.__table.shape[0] - 1):
            self.__edges.append((pos_to_index((i, 0), self.__tot_length),
                                 pos_to_index((i + 1, 0), self.__tot_length)))

        self.__indels[:, 0] = np.arange(0, self.__table.shape[0]) * self.__consec_indel_mult
        self.__indels[0, :] = np.arange(0, self.__table.shape[1]) * self.__consec_indel_mult

        if self.__heavy_consec_indels:
            print("hellooooo")
            # Write the first row and the first col
            self.__table[0, :] = range(0, -self.__table.shape[1], -1)
            self.__table[:, 0] = range(0, -self.__table.shape[0], -1)

    def __draw(self, draw_edges=False, tracebacks=None):
        """
        Draws a graph representation of the table, using NetworkX
        """
        self.__graph = nx.DiGraph()

        # Get nodes
        self.__nodes = get_nodes(self.__table.shape)
        self.__graph.add_nodes_from(self.__nodes)

        # Get positions
        self.__positions = get_positions(self.__nodes, self.__tot_length)

        # Draw edges
        if draw_edges:
            # If i'm tracebacking plot edges the opposite way
            if tracebacks is not None:
                self.__graph.remove_edges_from(self.__edges)
                self.__graph.add_edges_from(
                    [(edge[1], edge[0]) for edges in tracebacks["paths_edges"] for edge in edges])
            else:
                self.__graph.add_edges_from(self.__edges)

        self.__labels = get_labels(self.__table, self.__query, self.__reference)
        self.__colors = get_colors(self.__table, self.__query, self.__reference, self.__tracebacks)

        # Set nodes positions
        self.__layout = nx.spring_layout(self.__graph, pos=self.__positions, fixed=self.__nodes)

    def __print_graph(self):

        nx.draw(self.__graph, self.__layout,
                labels=self.__labels, node_color=self.__colors,
                font_size=12, node_size=500)

        plt.show()

    def __get_best_score(self, table, query, reference, current_position, pitch):
        """
        Get the best score out of the trielem
        """
        best_scores = []
        best_pos = []

        elements = get_trielem(current_position)
        curr_x, curr_y = current_position

        for j, position in enumerate(elements):

            # position of one of the possible source
            x, y = position

            # score of the possible source
            prev_score = table[x, y]

            # It's the "diagonal" move, you need to check equality
            if (j + 1) % 2 == 0:
                if reference[curr_y - 1] == query[curr_x - 1]:
                    # Match
                    update = self.__match_cost
                else:
                    # Mismatch
                    update = self.__miss_cost
            else:
                # Indel
                update = self.__indel_cost - (self.__indels[x, y] * self.__consec_indel_mult)
                print(update)
                if self.__heavy_consec_indels:
                    self.__indels[curr_x, curr_y] += 1

            # Update old score
            new_score = prev_score + update


            #
            best_scores.append(new_score)
            best_pos.append((x, y))
            #

            # # Check best score
            # if new_score > best_score:
            #     if best_score < 0:
            #         best_score = 0
            #     best_score = new_score
            #     best_pos = [(x, y)]
            #     print(f"best score for pos {current_position} is {best_score}")
            # # Need to remember all optimal edges
            # elif new_score == best_score:
            #     best_pos.append((x, y))

        arg_best_scores = np.argwhere(best_scores == np.amax(best_scores))
        print(arg_best_scores)
        best_pos = [best_pos[i[0]] for i in arg_best_scores]
        edges = []
        for pos in best_pos:
            edges.append((pos_to_index(pos, pitch),
                          pos_to_index(current_position, pitch)))

        best_score = np.max(best_scores)
        table[curr_x, curr_y] = round(best_score, 1) if best_score > 0 else 0

        return edges

    def align(self, draw=True):

        for x, row in enumerate(self.__table[1:], 1):
            for y, element in enumerate(row[1:], 1):
                self.__edges += self.__get_best_score(self.__table, self.__query, self.__reference, (x, y),
                                                      self.__tot_length)

        self.__draw(draw_edges=True)

        if draw:
            self.__print_graph()

        return self

    def traceback(self, draw=True):


        if self.__backtrack_mode == "NeedlemanWunsch":
            # Get all simple paths between bottom right and top left
            for path in nx.all_simple_paths(self.__graph, source=self.__tot_length + 1,
                                            target=self.__tot_length * self.__tot_height - 1):

                self.__tracebacks["paths"].append(path)

                edges = []
                for i in range(len(path) - 1):
                    edges.append((path[i], path[i + 1]))

                self.__tracebacks["paths_edges"].append(edges)

        elif self.__backtrack_mode == "SmithWaterman":
            src = argmax_to_index(np.argmax(self.__table), self.__table.shape[1], self.__tot_length)

            self.__recursive_visit(src)

            if draw:
                self.__print_graph()

        self.__draw(draw_edges=True, tracebacks=self.__tracebacks)

        if draw:
            self.__print_graph()

        return get_alignment(self.__query, self.__reference, self.__tracebacks, self.__tot_length)

    def __recursive_visit(self, current, nodes=[], edges=[]):

        nodes.append(current)
        if self.__labels[current] == "0.0":
            self.__tracebacks["paths"].append(nodes)
            self.__tracebacks["paths_edges"].append(edges)
            return

        for edge in self.__graph.in_edges(current):
            edges.append(edge)
            self.__recursive_visit(edge[0])

        # self.__tracebacks["paths"].append(nodes)
        # self.__tracebacks["paths_edges"].append(edges)