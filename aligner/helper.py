traceback_color = "#ff6262"
body_color = "#62efff"
header_color = "#ff8a50"
traceback_color = "#ff6262"


def get_colors(table, query, reference, traceback):
    """
    Helper function to create colors for NetworkX representation

    :param table: ndarray
    :param query: string
    :param reference: string
    :param traceback:

    :return: list of colors
    """
    query = [' '] + list(query)
    colors = (len(reference) + 2) * [header_color]

    for j in range(len(query)):
        colors += [header_color] + (table.shape[1]) * [body_color]

    if traceback is not None:
        for path in traceback["paths"]:
            for node in path:
                colors[node] = traceback_color

    return colors


def get_labels(table, query, reference):
    """
    Helper function to create lables for NetworkX representation

    :param table: ndarray
    :param query: string
    :param reference: string
    :return: list of labels
    """
    labels = {}

    names = [' ', ' '] + list(reference)
    query = [' '] + list(query)

    for j, base in enumerate(query):
        names.append(base)
        names += [str(elem) for elem in table[j, :]]

    # TODO: Remove double loop
    # Append them to the graph
    for j, text in enumerate(names):
        labels[j] = text

    return labels


def get_nodes(table_shape):
    """
    Returns a list of nodes (each node corresponds to its position in the list)
    :param table_shape: tuple
    :return: list of nodes
    """

    # For quality of life, the graph will contain also the header row and the header col containing respectively the
    # reference and the query string
    length = table_shape[1] + 1
    height = table_shape[0] + 1

    # Return a list of all nodes in numerical order
    return [n for n in range(length * height)]


def get_positions(nodes, pitch):
    """
    Converts the index in the nodes list to positions in space (NetworkX)

    :param nodes: list of nodes
    :param pitch: width of the graph
    :return: dict that associates a position for each node
    """
    positions = {}

    # Generate a dict with a position for each node
    for node in nodes:
        positions[node] = index_to_coord(node, pitch)

    return positions


def pos_to_index(pos, pitch):
    """
    Converts the position in the matrix to index in the node list
    :param pos: tuple
    :param pitch: int
    :return: The position in the node list
    """
    x, y = pos

    return (x + 1) * pitch + y + 1


def argmax_to_index(i, pitch_from, pitch_to):
    return i // pitch_from * pitch_to + pitch_to + i % pitch_from + 1

# def argmax_to_pos(i, pitch_from, pitch_to):
#     index = argmax_to_index(i, )
#     return index // pitch_to + 1, index % pitch_to + 1


def index_to_coord(i, pitch):
    return 1 + i % pitch, pitch - i // pitch


def index_to_pos(i, pitch):
    return i % pitch - 2, i // pitch - 2


def get_trielem(pos):
    x, y = pos
    left = (x - 1, y)
    top_left = (x - 1, y - 1)
    top = (x, y - 1)

    elems = [left, top_left, top]

    return elems


def get_nodes_positions(table_shape):
    """
    Helper function: generate nodes and positions from the graph table
    """
    positions = {}
    # Get a list of all nodes in numerical order
    nodes = [n for n in range(table_shape[0] * table_shape[1])]

    # Generate a dict with a position for each node
    for node in nodes:
        positions[node] = index_to_coord(node, table_shape[1])

    return nodes, positions


def get_alignment(query, reference, tracebacks, pitch):
    aligns = []

    # For all simple paths
    for path in tracebacks["paths"]:
        query_align = []
        reference_align = []

        # For each node in the path
        for i in range(len(path) - 1):

            # Get positions in the cost matrix
            pos = index_to_pos(path[i], pitch)
            next_pos = index_to_pos(path[i + 1], pitch)

            if pos[1] == next_pos[1]:
                query_align.append("-")
                reference_align.append(reference[next_pos[0]])

            elif pos[0] == next_pos[0]:
                query_align.append(query[next_pos[0]])
                reference_align.append("-")

            else:
                query_align.append(query[next_pos[1]])
                reference_align.append(reference[next_pos[0]])

        aligns.append((' '.join(query_align), ' '.join(reference_align)))

    return aligns
