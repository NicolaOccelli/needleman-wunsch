Needleman Wunsch Alignment
============================

Provides a quick easy-to-use interface to align 2 sequences, with the
possibility to plot the alignment and the traceback graph

Installing
-------------

.. code:: sh

    pip install needlemanwunsch-Nico995

Quick Example
-------------

.. code:: py

    from needlemanwunsch import nw

    query = "ACGC"
    reference = "GACTAC"
    my_aligner = nw.NW(query, reference)

    my_aligner.align(draw=False).traceback(draw=True)