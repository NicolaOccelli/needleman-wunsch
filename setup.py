from setuptools import setup, find_packages

with open("README.rst", "r") as fh:
    long_description = fh.read()

setup(
    name="needlemanwunsch-Nico995",
    version="0.1.0",
    author="Nicola Occelli",
    author_email="nicola.occelli@studenti.polito.it",
    description="Basic implementation of Needleman Wunsch alignment",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/NicolaOccelli/needleman-wunsch",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
